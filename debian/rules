#!/usr/bin/make -f
# -*- makefile -*-

export DH_VERBOSE=1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
       NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
       MAKEFLAGS += -j$(NUMJOBS)
endif

override_dh_auto_clean:
	[ ! -f Make.inc ] || dh_auto_clean

override_dh_auto_build:
	# dh_auto_install does both, compilation and installation.

override_dh_auto_install:
	VERBOSE=1 $(MAKE) install R=debian/tmp PACKAGE='debian'

override_dh_auto_test:
	# TODO: enable testing

override_dh_install:
	mv debian/tmp/usr/sbin/radiusd debian/tmp/usr/sbin/freeradius
	mv debian/tmp/usr/share/man/man8/radiusd.8 debian/tmp/usr/share/man/man8/freeradius.8
	# Not installed as we do not install the dhcpclient binary as per
	# https://github.com/FreeRADIUS/freeradius-server/issues/1734#issuecomment-247848277
	rm debian/tmp/usr/share/man/man1/dhcpclient.1
	# Remove all libtool .la files, as per
	# https://wiki.debian.org/ReleaseGoals/LAFileRemoval
	find debian/tmp/usr/lib/freeradius -name "*.la" -delete
	# Remove all plugin .a files (unnecessary), keep libfreeradius .a files
	# for end-users who want to statically link against libfreeradius.
	find debian/tmp/usr/lib/freeradius -name "*.a" -and \! -name "libfreeradius-*.a" -delete
	# We create the {mods,sites}-enabled links in freeradius-config.postinst
	# so that they are not re-created when users upgrade to a newer version.
	rm debian/tmp/etc/freeradius/3.0/mods-enabled/*
	rm debian/tmp/etc/freeradius/3.0/sites-enabled/*
	dh_install

override_dh_installpam:
	dh_installpam --name=radiusd

override_dh_installinit:
	dh_installinit --noscripts

override_dh_compress:
	dh_compress -Xexamples

override_dh_installdocs:
	dh_installdocs -Xdebian/tmp/usr/share/doc/freeradius/ChangeLog

override_dh_gencontrol:
	dh_gencontrol -- $(SUBSTVARS)

override_dh_auto_configure:
	dh_auto_configure -- $(confflags) \
		--config-cache \
		--disable-developer \
		--disable-openssl-version-check \
		--exec-prefix=/usr \
		--libdir=/usr/lib/freeradius \
		--datadir=/usr/share \
		--with-raddbdir=/etc/freeradius/3.0 \
		--with-logdir=/var/log/freeradius \
		--with-large-files \
		--with-udpfromto \
		--without-rlm_eap_tnc \
		--with-rlm_sql_postgresql_lib_dir=`pg_config --libdir` \
		--with-rlm_sql_postgresql_include_dir=`pg_config --includedir` \
		--with-iodbc-include-dir='/usr/include/iodbc' \
		--with-modules=rlm_python3 \
		--without-rlm_eap_ikev2 \
		--without-rlm_sql_oracle \
		--without-rlm_sql_unixodbc \
		--with-systemd \
		--with-radlast

%:
	dh $@
